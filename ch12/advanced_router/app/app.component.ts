import {Component} from '@angular/core';
import {RouterLink, Router, RouterOutlet} from '@angular/router';

// Base component
@Component({
selector: 'app-root',
template: `
<p>Select a component pair:</p>
<ul>

  <!-- First link uses value to set the route parameter -->
  <li><a [routerLink]="['/foo', val]">Parent: First component, path = /foo/11</a></li>

  <!-- Second link inserts second component -->
  <li><a routerLink='/bar'>Parent: Second component, path = /bar</a></li>
</ul>

<button (click)="handleClick()">Navigate to /foo/22</button>
<br/>

<!-- Outlet for primary routes -->
<router-outlet></router-outlet>
`})
export class AppComponent {

  private val: number;

  constructor(private router: Router) {
    this.val = 11;
  }

  public handleClick() {

    // Navigate programmatically
    this.router.navigate(['/foo', 2 * this.val],
      { queryParams: { color: 'red' } });
  }
}
