import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// First component
@Component({
selector: 'app-first',
template: `
<h2>I'm the first component!</h2>
`})
export class FirstComponent implements OnInit {

  constructor(private route: ActivatedRoute) {}

  public ngOnInit() {

    // Access route parameter
    const n: string = this.route.snapshot.params['num'];
    if (n) {

      // Display value of route parameter
      window.alert('Route parameter: ' + n);
    }
  }
}
