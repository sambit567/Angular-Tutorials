import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// Second component
@Component({
selector: 'app-second',
template: `
<h2>I'm the second component!</h2>
`})
export class SecondComponent implements OnInit {

  constructor(private route: ActivatedRoute) {}

  public ngOnInit() {

    // Access data from the route definition
    const msg: string = this.route.snapshot.data['message'];
    if (msg) {
      window.alert('Route definition data: ' + msg);
    }
  }
}
