import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { FirstComponent } from './first.component';
import { SecondComponent } from './second.component';
import { ThirdComponent } from './third.component';
import { FourthComponent } from './fourth.component';
import { ExtraComponent } from './extra.component';
import { ErrorComponent } from './error.component';

const routes: Routes = [
  {path: '', redirectTo: 'foo/baz', pathMatch: 'full'},
  {path: 'foo', component: FirstComponent,
    children: [
      { path: 'baz', component: ThirdComponent },
      { path: 'qux', component: FourthComponent }
    ]},
  {path: 'bar', component: SecondComponent,
    children: [
      { path: 'baz', component: FourthComponent },
      { path: 'qux', component: ThirdComponent }
    ]},
  {path: 'extraComp', component: ExtraComponent, outlet: 'extra'},
  {path: '**', component: ErrorComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    FourthComponent,
    ExtraComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
