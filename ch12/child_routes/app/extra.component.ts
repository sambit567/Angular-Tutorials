import {Component} from '@angular/core';

// Extra component
@Component({
selector: 'app-extra',
template: `
<button (click)="handleClick()">Extra Component</button>
`})
export class ExtraComponent {

  public handleClick() {
    window.alert('This component is associated with a secondary route.');
  }
}
