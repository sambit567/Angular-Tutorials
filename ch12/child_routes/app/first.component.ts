import {Component} from '@angular/core';

// First component
@Component({
selector: 'app-first',
template: `
<h2>I'm the first component!</h2>
<router-outlet></router-outlet>
`})
export class FirstComponent {}
