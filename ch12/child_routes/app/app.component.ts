import {Component} from '@angular/core';
import {RouterLink, Router, RouterOutlet} from '@angular/router';

// Base component
@Component({
selector: 'app-root',
template: `
<p>Select a component pair:</p>
<ul>
  <li><a routerLink='/foo/baz'>Parent: First component, Child: Third Component</a></li>

  <li><a routerLink='/foo/qux'>Parent: First component, Child: Fourth Component</a></li>

  <li><a routerLink='/bar/baz'>Parent: Second component, Child: Fourth Component</a></li>

  <li><a routerLink='/bar/qux'>Parent: Second component, Child: Third Component</a></li>

  <li><a [routerLink]="[{ outlets: { extra: ['extraComp'] } }]">
    Activate the secondary route</a></li>
</ul>
<br/>

<!-- Outlet for primary routes -->
<router-outlet></router-outlet>

<!-- Outlet for secondary route -->
<router-outlet name='extra'></router-outlet>
`})
export class AppComponent {
  constructor(private router: Router) {}
}

