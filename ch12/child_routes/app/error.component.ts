import {Component} from '@angular/core';

// Error component
@Component({
selector: 'app-error',
template: `
<h3>Path not recognized</h3>
`})
export class ErrorComponent {}
