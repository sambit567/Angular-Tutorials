import {Component} from '@angular/core';

// Second component
@Component({
selector: 'app-second',
template: `
<h2>I'm the second component!</h2>
<router-outlet></router-outlet>
`})
export class SecondComponent {}
