import {Component} from '@angular/core';

// Fourth component
@Component({
selector: 'app-fourth',
template: `
<h3>I'm the fourth component!</h3>
`})
export class FourthComponent {}
