import {Component} from '@angular/core';

// Third component
@Component({
selector: 'app-third',
template: `
<h3>I'm the third component!</h3>
`})
export class ThirdComponent {}
