import {Component} from '@angular/core';

// Second component
@Component({
selector: 'app-second',
template: `
I'm the second component!
`})
export class SecondComponent {}
