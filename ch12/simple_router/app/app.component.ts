import {Component} from '@angular/core';
import {RouterLink, Router, RouterOutlet} from '@angular/router';

// Base component
@Component({
selector: 'app-root',
template: `
<p>Select a component:</p>
<ul>
  <li><a routerLink='/foo'>First component</a></li>
  <li><a routerLink='/bar'>Second component</a></li>
</ul>
<router-outlet></router-outlet>
`})
export class AppComponent {
  constructor(private router: Router) {}
}
