import {Component} from '@angular/core';

// First component
@Component({
selector: 'app-first',
template: `
I'm the first component!
`})
export class FirstComponent {}
