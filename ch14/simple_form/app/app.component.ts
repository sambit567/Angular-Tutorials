import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
selector: 'app-root',
template: `

<!-- Associate the form with a FormGroup -->
<form [formGroup]='group' (ngSubmit)='handleSubmit()'>

  <!-- Associate the first text box with a FormControl -->
  First input:<input formControlName='input1'><br /><br />

  <!-- Associate the second text box with a FormControl -->
  Second input:<input formControlName='input2'><br /><br />

  <input type='button' (click)='changeFirst()' 
    value='Change First'><br />
  <input type='button' (click)='changeSecond()' 
    value='Change Second'><br />
  <input type='submit' value='Submit'>
</form>
`})

// Define the component's class
export class AppComponent {
  public group: FormGroup;
  constructor() {
    this.group = new FormGroup({
      'input1': new FormControl('init1'),
      'input2': new FormControl('init2')
    });
  }

  // Update the first control when the first button is clicked
  private changeFirst() {
    const control = this.group.get('input1');
    control.setValue(control.value + 'a');
  }

  // Update the second control when the second button is clicked
  private changeSecond() {
    const control = this.group.get('input2');
    control.setValue(control.value + 'z');
  }

 // Respond when the form is submitted
  private handleSubmit() {
    alert('Form submitted');
  };
}
