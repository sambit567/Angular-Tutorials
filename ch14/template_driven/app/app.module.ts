import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ErrorService } from './error.service';
import { ErrorMessageComponent } from './error-message.component';
import { ZipValidatorDirective } from './zip-validator.directive';
import { AcValidatorDirective } from './ac-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    ErrorMessageComponent,
    ZipValidatorDirective,
    AcValidatorDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [ErrorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
