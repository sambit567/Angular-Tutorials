import {Directive, Provider} from '@angular/core';
import {FormControl, NG_VALIDATORS} from '@angular/forms';

// Validates zip codes: 5-digit numbers
function zipValidator(fc: FormControl): {[errorCode: string]: boolean} {
  if ((fc.value != null) && (fc.value.match(/^\d{5}$/g))) {
    return null;
  } else {
    return {'zipcode': true};
  }
}

// Binds the zip code validator to NG_VALIDATORS
const zipValidatorBinding: Provider = {
  provide: NG_VALIDATORS,
  useValue: zipValidator,
  multi: true
};

@Directive({
  selector: '[validateZip]',
  providers: [zipValidatorBinding]
})
export class ZipValidatorDirective {}
