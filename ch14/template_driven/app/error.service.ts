import { Injectable } from '@angular/core';

// Returns messages for each error type
@Injectable()
export class ErrorService {

  public errorMap: {[errorCode: string]: string};

  constructor() {
    this.errorMap = {
      'required': 'This field is required',
      'zipcode': 'The zip code requires five digits',
      'areacode': 'The area code requires three digits'};
  }
}
