import {Directive, Provider} from '@angular/core';
import {FormControl, NG_VALIDATORS} from '@angular/forms';

// Validates area codes: 3-digit numbers
function acValidator(fc: FormControl): {[errorCode: string]: boolean} {
  if ((fc.value != null) && (fc.value.match(/^\d{3}$/g))) {
    return null;
  } else {
    return {'areacode': true};
  }
}

// Binds the area code validator to NG_VALIDATORS
const acValidatorBinding: Provider = {
  provide: NG_VALIDATORS,
  useValue: acValidator,
  multi: true
};

@Directive({
  selector: '[validateAreaCode]',
  providers: [acValidatorBinding]
})
export class AcValidatorDirective {}
