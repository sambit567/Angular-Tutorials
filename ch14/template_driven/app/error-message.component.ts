import {Component, Host, Input} from '@angular/core';
import {FormGroup, NgForm, NG_VALIDATORS} from '@angular/forms';

import {ErrorService} from './error.service';

// A component that reports errors
@Component({
selector: 'app-error',
template: `
<p *ngIf='msg !== null'>{{ msg }}</p>
` })
export class ErrorMessageComponent {

  @Input() private controlName: string;
  @Input() private errorChecks: string[];
  private errMap: {[errorCode: string]: string};
  private mainForm: NgForm;

  constructor(@Host() mainForm: NgForm, private service: ErrorService) {
    this.errMap = service.errorMap;
    this.mainForm = mainForm;
  }

  // Called when the msg property is accessed
  get msg() {

    const group: FormGroup = this.mainForm.form;
    const control = group.get(this.controlName);
    if (control && control.touched) {
      for (let i = 0; i < this.errorChecks.length; i++) {
        if (control.hasError(this.errorChecks[i])) {
          return this.errMap[this.errorChecks[i]];
        }
      }
    }
    return null;
  }
}
