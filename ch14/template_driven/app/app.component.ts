import {Component} from '@angular/core';
import {FormControl, FormGroup, NgForm, NG_VALIDATORS} from '@angular/forms';

@Component({
selector: 'app-root',
styleUrls: ['app.component.css'],
template: `
<form #f='ngForm' (ngSubmit)='submit()'>

  <div class='entry'>
    Enter a zip code:
    <input name='input1' [(ngModel)]='zipCode' required validateZip>
    <app-error controlName='input1' [errorChecks]="['required', 'zipcode']"></app-error>
  </div>

  <div class='entry'>
    Enter an area code:
    <input name='input2' [(ngModel)]='areaCode' required validateAreaCode>
    <app-error controlName='input2' [errorChecks]="['required', 'areacode']"></app-error>
  </div>

  <input type='submit' [disabled]='!f.form.valid' value='Submit'>
</form>
`})

// Define the component's controller
export class AppComponent {

  public zipCode: string;
  public areaCode: string;

  private submit() {
    alert('zip code: ' + this.zipCode + ', area code: ' + this.areaCode);
  }
}
