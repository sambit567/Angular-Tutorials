import {Component} from '@angular/core';
import {FormControl, FormGroup, FormArray, Validators} from '@angular/forms';

@Component({
selector: 'app-root',
styleUrls: ['app.component.css'],
templateUrl: 'app.component.html',
})

// Define the component's controller
export class AppComponent {

  private types = ['Carryout', 'Delivery'];
  private apps = ['Green beans - 1.50',
     'Cheese fries - 1.75', 'Tofu - 2.50'];
  private soups = ['Miso Soup - 3.25',
     'Beef soup - 3.80', 'Clam chowder - 4.25'];
  private meals = ['Fettucini - 17.80',
      'Chicken - 19.45', 'Lobster and Shrimp - 24.80'];
  private orders = [];
  private total = 0;
  private orderPlaced = false;
  private tip = false;
  private nameControl: FormControl;
  private emailControl: FormControl;
  private topGroup: FormGroup;
  private menuGroup: FormGroup;
  private mainGroup: FormGroup;

  constructor() {

    this.nameControl = new FormControl('John Smith',
      Validators.required);
    this.emailControl = new FormControl('jsmith@xyz.org',
      Validators.compose([Validators.required, Validators.email]));

    // Create group for ID controls
    this.topGroup = new FormGroup({
      'nameText': this.nameControl,
      'emailText': this.emailControl,
      'typeSelect': new FormControl(this.types[0]),
      'tipCheck' : new FormControl(this.tip)});

    // Create group for menu controls
    this.menuGroup = new FormGroup({
      'appSelect': new FormControl(this.apps[0]),
      'soupSelect': new FormControl(this.soups[0]),
      'mealSelect': new FormControl(this.meals[0])});

    // Create group to hold subgroups
    this.mainGroup = new FormGroup({
      'top': this.topGroup,
      'menu': this.menuGroup});
  }

  // Set whether a gratuity will be added
  private changeTip(t: boolean): void {
    this.tip = t;
    if (t) {
      this.total *= 1.15;
    } else {
      this.total /= 1.15;
    }
  }

  // Add a new item to the order
  private addOrderItem(item: string): void {
    this.orders.push(item);
    const pos = item.indexOf('-') + 1;
    if (this.tip) {
      this.total += 1.15 * Number(item.substring(pos));
    } else {
      this.total += Number(item.substring(pos));
    }
  }

  // Remove an item from the order
  private deleteOrderItem(item: string, index: number): void {
    this.orders.splice(index, 1);
    const pos = item.indexOf('-') + 1;
    if (this.tip) {
      this.total -= 1.15 * Number(item.substring(pos));
    } else {
      this.total -= Number(item.substring(pos));
    }
  }

  private handleSubmit(): void {
    let outMsg = 'Total charge is ' + this.total.toFixed(2) + ': ';
    let pos: number;
    for (let i = 0; i < this.orders.length; i++) {
      pos = this.orders[i].indexOf('-') - 1;
      outMsg += this.orders[i].substring(0, pos) + ', ';
    }
    outMsg = outMsg.substring(0, outMsg.length - 2);
    alert(outMsg);
  };
}
