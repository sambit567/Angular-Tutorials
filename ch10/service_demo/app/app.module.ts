import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BookArrayService } from './bookarray.service';
import { BookService } from './book.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ BookArrayService, BookService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
