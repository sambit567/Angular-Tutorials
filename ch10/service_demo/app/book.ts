export class Book {
  constructor(public author: string,
              public title: string,
              public numPages: number) {}
}
