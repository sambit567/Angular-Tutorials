import { Injectable } from '@angular/core';

import { Book } from './book';
import { BookArrayService } from './bookarray.service';

// A service class that uses dependency injection
@Injectable()
export class BookService {

  public goodBooks: Book[];

  constructor(bookArray: BookArrayService) {
    this.goodBooks = bookArray.getBookArray();
  }

  public getBooks() {
    return this.goodBooks;
  }
}
