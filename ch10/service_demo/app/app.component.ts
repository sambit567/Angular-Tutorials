import { Component, Injectable } from '@angular/core';

import { Book } from './book';
import { BookService } from './book.service';

@Component({
selector: 'app-root',
template: `
<ul>
  <li *ngFor = 'let book of goodBooks; let i = index'>
    Good book {{ i }}: {{ book.title }} by {{ book.author }}
  </li>
</ul>
`})

// The component receives the injected BookService
export class AppComponent {

  private goodBooks: Book[];

  constructor(private bookService: BookService) {
    this.goodBooks = bookService.getBooks();
  }
}
