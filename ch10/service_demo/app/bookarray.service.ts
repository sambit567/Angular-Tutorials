import { Injectable } from '@angular/core';
import { Book } from './book';

// Provide book array to the book service
@Injectable()
export class BookArrayService {

  public books: Book[] = [
    new Book('Melville', 'Moby Dick', 544),
    new Book('Austen', 'Persuasion', 150),
    new Book('Brontë', 'Wuthering Heights', 212)
  ];

  // Return the array of books
  public getBookArray() {
    return this.books;
  }
}
