import {Component, ClassProvider, FactoryProvider, ReflectiveInjector} from '@angular/core';

class TokenA {}
class TokenB {}
class Other {}
class Thing {}

@Component({
selector: 'app-root',
template: `
<ul>
<li>ResultA is an instance of Other: {{ resultA }}</li>
<li>ResultB is an instance of Thing: {{ resultB }}</li>
</ul>
`})

export class AppComponent {

  private resultA: boolean;
  private resultB: boolean;

  constructor() {

    // Class provider
    const providerA: ClassProvider = {
      provide: TokenA, useClass: Other};
    const providerB: FactoryProvider = {
      provide: TokenB, useFactory: () => new Thing() };

    // Create an injector from all three providers
    const injector = ReflectiveInjector.resolveAndCreate(
      [providerA, providerB]);

    // Invoke get and examine results
    this.resultA = injector.get(TokenA) instanceof Other;
    this.resultB = injector.get(TokenB) instanceof Thing;
  }
}
