import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ButtonDirective } from './button.directive';
import { LoopDirective } from './loop.directive';

@NgModule({
  declarations: [
    AppComponent,
    ButtonDirective,
    LoopDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
