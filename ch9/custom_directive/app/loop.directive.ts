import {Directive, ViewContainerRef, TemplateRef, OnChanges,
  Input, SimpleChange} from '@angular/core';

// This directive inserts multiple elements into the DOM
@Directive({selector: '[addElements]'})
export class LoopDirective implements OnChanges {

  private context: LoopContext = new LoopContext();
  private container: ViewContainerRef;
  private templateRef: TemplateRef<LoopContext>;

  constructor(container: ViewContainerRef,
    templateRef: TemplateRef<LoopContext>) {

    this.container = container;
    this.templateRef = templateRef;
  }

  @Input()
  set addElements(num: any) {
    this.context.numElements = num;
  }

  // Respond each time the property's value is set
  public ngOnChanges(changes: {[key: string]: SimpleChange}) {
    for (let i = 0; i < this.context.numElements; i++) {
      this.container.createEmbeddedView(this.templateRef);
    }
  }
}

export class LoopContext {
  public numElements: number = null;
}
