import {Component} from '@angular/core';

// This component accesses both directives
@Component({
selector: 'app-root',
template: `
<ul>
  <li *addElements='num'>List item</li>
</ul>
<button removeChar>Click to remove characters</button>
`})
export class AppComponent {
  private num = 3;
}
