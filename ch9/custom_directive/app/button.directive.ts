import {Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({selector: '[removeChar]'})
export class ButtonDirective {

  // Access the directive's element
  constructor(private ref: ElementRef) {}

  // Handle click events
  @HostListener('click') handleClick() {

    // Remove the last character from the element's text
    const str = this.ref.nativeElement.innerText;
    if (str.length > 1) {
      this.ref.nativeElement.innerText =
        str.substring(0, str.length - 1);
    }
  }
}
