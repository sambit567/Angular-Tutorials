class DomTreeAnalyzer {

  public static analyze(children: NodeList) {
    for (let i = 0; i < children.length; ++i) {
      const node = children[i];

      // Print details of each node
      if (node.nodeType === Node.ELEMENT_NODE) {
        console.log("Name: " + node.nodeName);
        console.log("Parent name: " + node.parentNode.nodeName);
        DomTreeAnalyzer.analyze(node.childNodes);
      }
    }
  }
}

// Start with the children of the overall document
DomTreeAnalyzer.analyze(document.childNodes);
