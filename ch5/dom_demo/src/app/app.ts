// Create a form and add it to the document's body
let form = document.createElement("form");
form.action = "./recipient.html";
document.body.appendChild(form);

// Create two labels
let label1 = document.createElement("label");
label1.textContent = "First name:";
let label2 = document.createElement("label");
label2.textContent = "Last name:";

// Create two input elements
let input1 = document.createElement("input");
input1.name = "fname";
input1.required = true;
let input2 = document.createElement("input");
input2.name = "lname";
input2.required = true;

// Create a submit button
let button = document.createElement("button");
button.type = "submit";
button.textContent = "Submit";

// Add the form's elements to the form
form.appendChild(label1); form.appendChild(input1);
form.appendChild(document.createElement("br"));
form.appendChild(label2); form.appendChild(input2);
form.appendChild(document.createElement("br"));
form.appendChild(button);
