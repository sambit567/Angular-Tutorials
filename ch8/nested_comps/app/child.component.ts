import {Component} from '@angular/core';

// The child component
@Component({
selector: 'child-comp',
template: `
<li>
  Child <ng-content></ng-content>
</li>
`})
export class ChildComponent {}
