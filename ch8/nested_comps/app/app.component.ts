import {Component, QueryList, ViewChildren, AfterViewInit} from '@angular/core';

import { ChildComponent } from './child.component';
import { ParentComponent } from './parent.component';

// The top-level component
@Component({
selector: 'app-root',
template: ` 
<parent-comp [parentProp]='message'>
  <child-comp>Content1</child-comp>
  <child-comp>Content2</child-comp>
  <child-comp>Content3</child-comp>  
</parent-comp>
`})
export class AppComponent implements AfterViewInit {

 // Declare properties
  @ViewChildren(ParentComponent) public children: QueryList<ParentComponent>;
  public message = 'Hello';

  public ngAfterViewInit() {
    alert('The top-level component has ' +
      this.children.length + ' view child');
  }
}
