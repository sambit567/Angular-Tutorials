import {Component, ContentChildren, Input, AfterContentInit, QueryList} from '@angular/core';

import { ChildComponent } from './child.component';

// The parent component
@Component({
selector: 'parent-comp',
template: `
<p>There are {{ children.length }} children:</p>
<ol>
  <ng-content></ng-content>
</ol>
<p>The parent's msg property equals {{ msg }}.</p>
`})
export class ParentComponent implements AfterContentInit {
  @Input('parentProp') public msg: string;
  @ContentChildren(ChildComponent) public children: QueryList<ChildComponent>;

  public ngAfterContentInit() {
    alert('The parent component has ' +
      this.children.length + ' content children');
  }
}
