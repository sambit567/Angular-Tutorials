import {Component} from '@angular/core';

@Component({
selector: 'app-root',
template: `
<button type='button' (click) = 'incrNumClicks()'
                      (mouseenter) = 'setMouseEnter()'
                      (mouseleave) = 'setMouseLeave()'>
  Number of clicks: {{ numClicks }}<br />
  Mouseover: {{ mouseOver }}
</button>
`})

// Define the component's class
export class AppComponent {

  // Declare and initialize properties
  public numClicks = 0;
  public mouseOver = false;

  // Respond to user events
  public incrNumClicks() {
    this.numClicks += 1;
  }
  public setMouseEnter() {
    this.mouseOver = true;
  }
  public setMouseLeave() {
    this.mouseOver = false;
  }
}
