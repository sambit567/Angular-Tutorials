import {Component} from '@angular/core';

@Component({
selector: 'app-root',

// Define styles for the template elements
styles:
['.classselector div { display: block; margin-bottom: 10px; }',
 '.classselector .first { font-weight:normal; color:purple; }',
 '.classselector .second { font-weight:bold; color:orange; }',
 '.classselector .third { font-style:italic; color:brown; }'],

 // Define the component's appearance
template: `
<div class='classselector'>
  <div>
    <label>Class: </label>
    <select #sel>
      <option>first</option>
      <option>second</option>
      <option>third</option>
    </select>
  </div>
  <div>
    <button (click)='handleClick(sel.value)'>
      Apply selection
    </button><br />
  </div>
  <div>
    <label [className]='labelClass'>{{ labelText }}</label>
  </div>
</div>
`})

// Define the component's class
export class AppComponent {

  // Declare and initialize
  public labelClass = '';
  public labelText =
    'Please select a class and press the button.';

  // Select the class and the label's text
  public handleClick(choice: string) {
    this.labelClass = choice;
    if (choice === 'first') {
      this.labelText =
        'The first class prints normal text in purple.';
    } else if (choice === 'second') {
      this.labelText =
        'The second class prints boldface text in orange.';
    } else if (choice === 'third') {
      this.labelText =
        'The third class prints italic text in brown.';
    }
  }
}
