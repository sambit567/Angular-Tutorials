// Superclass
class A {
  public addConstant(num: number): number {
    return num + 3;
  }
}

// Subclass
class B extends A {
  public addConstant(num: number): number {
    return num + 7;
  }
}

let instA = new A();
let ans1 = instA.addConstant(5);   // ans1 = 5 + 3 = 8

let instB = new B();
let ans2 = instB.addConstant(5);   // ans2 = 5 + 7 = 12
