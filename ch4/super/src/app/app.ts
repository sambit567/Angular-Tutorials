// Superclass
class A {

  public returnNum(): number {
    return 3;
  }
}

// Subclass
class B extends A {

  public returnNum(): number {
    return 5;
  }

  public addNums(): number {
    return this.returnNum() + super.returnNum();
  }
}

let instB = new B();
let ans = instB.addNums();    // ans = 5 + 3 = 8
