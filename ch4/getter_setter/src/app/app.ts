class A {
  private num: number;
  constructor() { this.num = 5; }

  // Getter method
  get foo(): number { return this.num; }

  // Setter method
  set foo(f: number) { this.num = f; }
}

let objA = new A();
let ans1 = objA.foo;     // Calls foo(), ans1 = 5

objA.foo = 9;            // Calls foo(9)
let ans2 = objA.foo;     // Calls foo(), ans2 = 9
