// First class to be mixed in
class AddConstant {

  public num1: number;
  public addNum(n: number): number {
    return n + this.num1;
  }
}

// Second class to be mixed in
class SubtractConstant {

  public num2: number;
  public subtractNum(n: number): number {
    return n - this.num2;
  }
}

// Class to receive mixins
class AddSubtract implements AddConstant, SubtractConstant {

  // Declare properties
  public num1 = 5;
  public num2 = 7;

  // Declare methods using function types
  public addNum: (n: number) => number;
  public subtractNum: (n: number) => number;

  public addSubtract(x: number) {
    return this.addNum(x) + this.subtractNum(x);
  }
}

// Mix the classes together
applyMixins(AddSubtract, [AddConstant, SubtractConstant]);

// Define the applyMixins function
function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach((baseCtor) => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}

let test = new AddSubtract();
let ans = test.addSubtract(9);  // (9 + 5) + (9 - 7) = 16
