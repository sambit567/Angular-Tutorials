// An interface with a property and two methods
interface AddSubtractConstant {
  k: number;

  addConstant(num: number): number;
  subtractConstant(num: number): number;
}

// A class that implements the interface
class AddSubtractConstantImpl implements AddSubtractConstant {
  public k = 12;

  public addConstant(num: number): number {
    return num + this.k;
  }

  public subtractConstant(num: number): number {
    return num - this.k;
  }
}
