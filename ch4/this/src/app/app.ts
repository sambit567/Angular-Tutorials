class Adder {

  // Return a new Adder instance
  constructor(private x: number, private y: number) {}

  public addOnce(): number {
    return this.x + this.y;
  }

  public addTwice(): number {
    return this.addOnce() + this.addOnce();
  }
}

let adderInstance = new Adder(5, 2);
let ans = adderInstance.addTwice();     // ans = 14
