import { browser, protractor, $, $$ } from 'protractor';

describe('the protractor_demo project', function() {

  // Initialize test
  beforeEach(() => {
    browser.get('/');
  });

  // Make sure the button is present
  it('should have a button whose ID is demo_button', function() {
    expect($$('#demo_button').count()).toEqual(1);
  });

  // Click the button twice and count the clicks
  it('should have a demo_button whose text identifies the number of clicks', function() {
    browser.actions().click($('#demo_button')).click($('#demo_button')).perform();
    expect($('#demo_button').getText()).toEqual('2');
  });

  // Add text to the input box and verify its content
  it('should have a demo_input whose text equals HELLOhello', function() {
    $('#demo_input').sendKeys(protractor.Key.SHIFT, 'hello', protractor.Key.NULL, 'hello');
    expect($('#demo_input').getAttribute('value')).toEqual('HELLOhello');
  });
});
