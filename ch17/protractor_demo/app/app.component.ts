import {Component} from '@angular/core';

@Component({
selector: 'app-root',
template: `
<button id='demo_button' 
  type='button' (click) = 'incrNumClicks()'>
  {{ numClicks }}
</button>
<input id='demo_input' type='text' size='10'>
`})

// Define the component's class
export class AppComponent {

  private numClicks = 0;

  // Respond to user events
  private incrNumClicks() {
    this.numClicks += 1;
  }
}
