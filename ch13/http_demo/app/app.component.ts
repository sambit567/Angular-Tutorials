import {Component} from '@angular/core';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
selector: 'app-root',
template: `
<p>Authors:</p>
<table>
  <tr *ngFor='let author of authors'>
    <td>{{ author.first }}</td>
    <td>{{ author.last }}</td>
  </tr>
</table>
`})
export class AppComponent {
  private authors: Object[];
  private opts: RequestOptionsArgs;

  constructor(private http: Http) {

    // Create the query string
    this.opts = {search: 'query=authors'};

    // Send a request and process the response
    this.http.get('http://www.ngbook.io/http_demo', this.opts)
      .map((res: Response) => res.json())
      .subscribe((authors: Array<Object>) => this.authors = authors);
  }
}
