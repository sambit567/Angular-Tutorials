import {Component} from '@angular/core';
import {Jsonp, RequestOptionsArgs, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
selector: 'app-root',
template: `
<p>Authors:</p>
<table>
  <tr *ngFor='let author of authors'>
    <td>{{ author.first }}</td>
    <td>{{ author.last }}</td>
  </tr>
</table>
`})
export class AppComponent {
  private authors: Object[];
  private opts: RequestOptionsArgs;

  constructor(private jsonp: Jsonp) {

    // Create the query string
    this.opts = {search: 'CALLBACK=process'};

    // Transfer data using JSONP
    this.jsonp.get('http://www.ngbook.io/jsonp_demo', this.opts)
         .subscribe((res: Response) => this.authors = res.json());
  }
}
