describe("The example", () => {

  // Initialize variable
  let example: ExampleClass;
  beforeEach(() => { example = new ExampleClass(); });

  // Perform tests
  it("should be colored red", () => {
    expect(example.color).toBe("red"); });

  it("and the number should be greater than 4", () => {
    expect(example.number).toBeGreaterThan(4); });

  it("and the truth property should be true", () => {
    expect(example.truth).toBeTruthy(); });
});
