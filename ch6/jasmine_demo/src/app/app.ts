class ExampleClass {
  public color: string;
  public number: number;
  public truth: boolean;

  constructor() {
    this.color = "red";
    this.number = 6;
    this.truth = true;
  }
}
