describe("Test the ExampleClass", () => {

  // Initialize variable
  let example: ExampleClass;
  beforeEach(() => { example = new ExampleClass(); });

  // Perform tests
  it("Test color", () => {
    expect(example.color).toBe("red");
  });
  it("Test number", () => {
    expect(example.number).toBeGreaterThan(4);
  });
  it("Test truth", () => {
    expect(example.truth).toBeTruthy();
  });
});
