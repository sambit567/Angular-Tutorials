module.exports = function(config) {
  config.set({
    
    // Testing framework to be executed
    frameworks: ["jasmine"],
    
    // Browsers to launch tests with
    browsers: ["Chrome"],    
    
    // Files to load in the browser
    files: ["../app/app.js", "./test.js"],
    
    // Should Karma run its tests and exit?
    singleRun: true,
    
    // Should the files be monitored for changes?
    autoWatch: false,
  })
}
