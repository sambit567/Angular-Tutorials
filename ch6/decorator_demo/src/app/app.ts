// No annotations
class Book {
  constructor(public author: string, public title: string,
              public numberOfPages: number) {}

  public displayNumPages(): string {
    return this.numberOfPages.toString();
  }
}

// With annotations
@classChange
class DecoratedBook {

  @changeProp
  public author: string;
  public title: string;
  public numberOfPages: number;

  constructor(public a: string, t: string,
              @paramChange n: number) {
    this.author = a;
    this.title = t;
    this.numberOfPages = n;
  }

  @methodChange
  public displayNumPages(): string {
    return this.numberOfPages.toString();
  }
}

// Modify a property
function changeProp(target: any, key: string) {

  let prop = target[key];

  // The getter method
  const getFunc = () => {
    prop += ", Esquire";
    return prop;
  };

  // The setter method
  const setFunc = (newProp: string) => {
    prop = newProp;
    return prop;
  };

  // Delete existing property
  if (delete target[key]) {

    // Create new property
    Object.defineProperty(target, key, {
      configurable: true,
      enumerable: true,
      get: getFunc,
      set: setFunc});
  }
}

// Modify the class's constructor
function classChange(oldFunc: any): any {

  // Define the new constructor function
  const newFunc = (...args: any[]) => {

    // Change the constructor arguments
    const newObj: any = function() {
      if (args.length === 3) {
        args[2] = 400;
      }
      return oldFunc.apply(this, args);
    };
    newObj.prototype = oldFunc.prototype;
    return new newObj();
  };

  // Ensure that instanceof will work properly
  newFunc.prototype = oldFunc.prototype;
  return newFunc;
}

// Modify a method
function methodChange(target: object, key: string, desc: any) {

  // Store original function
  const origMethod = desc.value;

  // Define new function
  desc.value = function(...args: any[]) {
    const result = origMethod.apply(this, args);
    return "more than ".concat(result);
  };
  return desc;
}

// Add a property to the class
function paramChange(target: any, key: string, index: number) {
  target.prototype.publisher = "Harper and Brothers";
}

// Create objects
let b = new Book("Herman Melville", "Moby Dick", 544);
let db = new DecoratedBook("Herman Melville", "Moby Dick", 544);

// Display results
document.writeln("The book ".concat(b.title)
  .concat(" was written by ")
  .concat(b.author).concat(" and has ")
  .concat(b.displayNumPages()).concat(" pages. ")
  .concat("The publisher is ").concat(b["publisher"])
  .concat(".<br />"));
document.writeln("The book ".concat(db.title)
  .concat(" was written by ")
  .concat(db.author).concat(" and has ")
  .concat(db.displayNumPages()).concat(" pages. ")
  .concat("The publisher is ")
  .concat(db["publisher"]).concat("."));
