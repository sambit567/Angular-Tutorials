export class SubtractNumbers {

  constructor(private x: number, private y: number) {}

  public printResult(): number {
    return this.x - this.y;
  }
}
