import { AddNumbers } from "./AddNumbers";
import { SubtractNumbers } from "./SubtractNumbers";

const addNumbers: AddNumbers = new AddNumbers(3, 5);
document.write(addNumbers.printResult().toString());

const subNumbers: SubtractNumbers = new SubtractNumbers(9, 4);
document.write(subNumbers.printResult().toString());
