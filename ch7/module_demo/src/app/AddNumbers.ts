export class AddNumbers {

  constructor(private x: number, private y: number) {}

  public printResult(): number {
    return this.x + this.y;
  }
}
