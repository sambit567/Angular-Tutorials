import {Component, Pipe, PipeTransform} from '@angular/core';

// Access the pipe as 'orderBy'
@Pipe({ name: 'orderBy' })
export class OrderByPipe implements PipeTransform {

  private result: any[] = [];

  public transform(arr: any, args: any[] = []) {

    // Make sure array is an input
    if (Object.prototype.toString.call(arr) === '[object Array]') {

      // Clone input array
      this.result = arr.slice(0);

      // No arguments - sort primitives ascending
      if (args.length === 0) {
        this.sortPrimitives(true);

      // The first argument is a function
      } else if (Object.prototype.toString.call(args[0]) === '[object Function]') {
        this.sortByFunction(args[0]);

      // The first argument is a string
      } else if (Object.prototype.toString.call(args[0]) === '[object String]') {
        this.sortByString(args[0]);

      // The first argument is an array
      } else if (Object.prototype.toString.call(args[0]) === '[object Array]') {
        this.sortByArray(args[0]);
      }

      // If necessary, reverse the array
      if ((args.length === 2) && (args[1])) {
        return this.result.reverse();
      } else {
        return this.result;
      }
    }

    // If not an array, return the original input
    return arr;
  }

  // Sort an array containing primitives
  private sortPrimitives(ascending: boolean) {

    // Sort numbers in array
    if (typeof(this.result[0]) === 'number') {
      if (ascending) {
        return this.result.sort((x: number, y: number) => {
          return x - y;
        });
      } else {
        return this.result.sort((x: number, y: number) => {
          return y - x;
        });
      }

    // Sort strings (or whatever)
    } else {
      this.result.sort();
      if (!ascending) {
        this.result.reverse();
      }
      return this.result;
    }
  }

  // Sort values by a function
  private sortByFunction(func: Function) {
    this.result.sort((x: any, y: any) => {
      return func(x) - func(y);
    });
  }

  // Sort numbers in ascending order
  private sortByString(arg: string) {

    let ascending = true;

    // Process sort direction character
    if (arg.charAt(0) === '-') {
      arg = arg.slice(1);
      ascending = false;
    } else if (arg.charAt(0) === '+') {
      arg = arg.slice(1);
    }

    // No property name - primitive array
    if (arg.length === 0) {
      this.sortPrimitives(ascending);

    // Process numeric properties
    } else if (typeof(this.result[0][arg]) === 'number') {

      if (ascending) {
        return this.result.sort((x: any, y: any) => {
          return x[arg] - y[arg];
        });
      } else {
        return this.result.sort((x: any, y: any) => {
          return y[arg] - x[arg];
        });
      }

    // Process string properties
    } else if (ascending) {
      this.result.sort((x: any, y: any) => {
        if (x[arg] < y[arg]) { return -1; }
        if (x[arg]  > y[arg]) { return 1; }
        return 0;
      });
    } else {
      this.result.sort((x: any, y: any) => {
        if (x[arg] < y[arg]) { return 1; }
        if (x[arg]  > y[arg]) { return -1; }
        return 0;
      });
    }
  }

  // Sort numbers in ascending order
  private sortByArray(array: any[]) {

    if (array.length === 0) {
      this.sortPrimitives(true);
    } else {

      // Process successive elements of array
      this.result.sort((x: any, y: any) => {

        for (let i = 0; i < array.length; i++) {
          let ascending = true;
          let arg = array[i];

          // Process sort direction character
          if (arg.charAt(0) === '-') {
            arg = arg.slice(1);
            ascending = false;
          } else if (arg.charAt(0) === '+') {
            arg = arg.slice(1);
          }

          // Process numeric property
          if (typeof(x[arg]) === 'number') {

            const diff = x[arg] - y[arg];
            if (ascending) {
              if (diff < 0) { return -1; }
              if (diff > 0) { return 1; }
            } else {
              if (diff < 0) { return 1; }
              if (diff > 0) { return -1; }
            }

            // Process string properties
          } else if (ascending) {
            if (x[arg] < y[arg]) { return -1; }
            if (x[arg]  > y[arg]) { return 1; }
          } else {
            if (x[arg] < y[arg]) { return 1; }
            if (x[arg]  > y[arg]) { return -1; }
          }
        }
        return 0;
      });
    }
  }
}
