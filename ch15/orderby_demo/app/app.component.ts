import {Component} from '@angular/core';

// Define the component's markup
@Component({
selector: 'app-root',
templateUrl: 'app.component.html'
})

// Define the component's controller
export class AppComponent {
  private numArray = [72, 103, -1.6, 17.9, 65, -8.2];
  private stringArray = ['this', 'array', 'has', 'five', 'words'];
  private objArray = [{color: 'red', letter: 'b', num: 12}, {color: 'yellow', letter: 'c', num: 25},
    {color: 'red', letter: 'a', num: 9}, {color: 'green', letter: 'b', num: 17}];
}
