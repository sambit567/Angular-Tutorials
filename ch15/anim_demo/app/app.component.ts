import {Component} from '@angular/core';
import {trigger, state, style, animate, transition, keyframes} from '@angular/animations';

@Component({
selector: 'app-root',
animations: [
  trigger('text1', [
    state('start', style({ 'margin-left': '0px' })),
    state('end', style({ 'margin-left': '400px' })),
    transition('* => *', [ animate('2s ease-in') ])
  ]),
  trigger('text2', [
    state('start', style({ 'margin-left': '0px' })),
    state('end', style({ 'margin-left': '400px' })),
    transition('* => *', [ animate('2s ease-out') ])
  ]),
  trigger('text3', [
    state('start', style({ 'margin-left': '0px' })),
    state('end', style({ 'margin-left': '400px' })),
    transition('* => *', [ animate('2s ease-in-out') ])
  ]),
  trigger('text4', [
    state('start', style({ 'margin-left': '0px' })),
    state('end', style({ 'margin-left': '400px' })),
    transition('start => end', [ animate('2s',
      keyframes([
        style({ 'margin-left': '0px', 'offset': 0.1 }),
        style({ 'margin-left': '250px', 'offset': 0.3 }),
        style({ 'margin-left': '400px', 'offset': 1.0 })
      ])
    )])
  ]),
  trigger('rot', [
    state('start', style({ '-ms-transform': 'rotate(0deg)',
      '-webkit-transform': 'rotate(0deg)', 'transform': 'rotate(0deg)' })),
    state('cw', style({ '-ms-transform': 'rotate(180deg)',
      '-webkit-transform': 'rotate(180deg)', 'transform': 'rotate(180deg)' })),
    state('ccw', style({ '-ms-transform': 'rotate(-180deg)',
      '-webkit-transform': 'rotate(-180deg)', 'transform': 'rotate(-180deg)' })),
    transition('* => *', [ animate('1500ms') ])
  ])
],
template: `
<p [@text1]='state1' (@text1.done)='moveText1()'>EASE IN</p>
<p [@text2]='state2' (@text2.done)='moveText2()'>EASE OUT</p>
<p [@text3]='state3' (@text3.done)='moveText3()'>EASE-IN-OUT</p>
<p [@text4]='state4' (@text4.done)='moveText4()'>KEYFRAMES</p>
<br/><br/><br/>
<div style='width:140px;' [@rot]='rotState' (@rot.done)='rotateText()'>TEXT ROTATION</div>
`})

// Define the component's class
export class AppComponent {

  public state1 = 'start';
  public state2 = 'start';
  public state3 = 'start';
  public state4 = 'start';
  public rotState = 'start';

  // Move the first paragraph horizontally
  public moveText1() {
    this.state1 = (this.state1 === 'start' ? 'end' : 'start');
  }

  // Move the second paragraph horizontally
  public moveText2() {
    this.state2 = (this.state2 === 'start' ? 'end' : 'start');
  }

  // Move the third paragraph horizontally
  public moveText3() {
    this.state3 = (this.state3 === 'start' ? 'end' : 'start');
  }

  // Move the fourth paragraph horizontally
  public moveText4() {
    this.state4 = (this.state4 === 'start' ? 'end' : 'start');
  }

  // Rotate text
  public rotateText() {

    if (this.rotState === 'start') {
      this.rotState = 'cw';
    } else if (this.rotState === 'cw') {
      this.rotState = 'ccw';
    } else  {
      this.rotState = 'cw';
    }
  }
}
