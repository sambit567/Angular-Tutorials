import {Component} from '@angular/core';

// Define the component's markup
@Component({
selector: 'app-root',
template: `
<md-list>
  <md-list-item>
    <img md-list-avatar src='assets/images/smiley.jpg'>
    <p md-line>First Smiley</p>
    <p md-line>This item contains the first smiley</p>
  </md-list-item>
  <md-list-item>
    <img md-list-avatar src='assets/images/smiley.jpg'>  
    <p md-line>Second Smiley</p>
    <p md-line>This item contains the second smiley</p>
  </md-list-item>
  <md-list-item>
    <img md-list-avatar src='assets/images/smiley.jpg'>  
    <p md-line>Third Smiley</p>
    <p md-line>This item contains the third smiley</p>
  </md-list-item>  
</md-list>
`})

// Define the component's controller
export class AppComponent {
}
