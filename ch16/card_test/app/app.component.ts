import {Component} from '@angular/core';

@Component({
selector: 'app-root',
styles: ['md-card { width: 300px; }'],
template: `
<md-card>
  <md-card-header>
    <img md-card-avatar src='assets/images/smiley.jpg'>
    <md-card-title>Don Diego de la Vega</md-card-title>
    <md-card-subtitle>Posted at midnight</md-card-subtitle>
  </md-card-header>
  <md-card-content>
    Out of the night, when the full moon is bright, comes a horseman known as Zorro. 
    This bold renegade carves a Z with his blade, a Z that stands for Zorro!
  </md-card-content>
  <md-card-actions>
    <button md-raised-button (click)='handleClick()'>Like</button>
  </md-card-actions>
</md-card>
`})

// The base component
export class AppComponent {
  private handleClick() {
    alert('Hey, I like you too!');
  }
}
