import {Component} from '@angular/core';

@Component({
selector: 'app-root',
styles: ['md-hint { font-weight: bold; font-size: 80%; }'],
template: `
<md-input-container>

  <input mdInput placeholder='{{msg}}' maxLength='20' dividerColor='accent' #inp>
    
  <md-hint>
    {{ inp.value.length < 7 ? 'More characters needed' : 'Hooray!' }}
  </md-hint>

</md-input-container>
`})

export class AppComponent {
  private msg = 'Enter 7 or more characters:';
}
