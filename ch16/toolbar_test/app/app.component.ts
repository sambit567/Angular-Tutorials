import {Component} from '@angular/core';

@Component({
selector: 'app-root',
styles: ['.fill_horizontal { flex: 1 1 auto; }'],
template: `
<md-toolbar color='primary'>
  Application Title
  <span class='fill_horizontal'></span>

  <!-- Create the menu button -->
  <input type='image' src='assets/images/menu.png' 
    (click)='handleClick()'>

  <!-- Create toolbar rows -->
  <md-toolbar-row>Row 1</md-toolbar-row>
  <md-toolbar-row>Row 2</md-toolbar-row>

</md-toolbar>
`})

export class AppComponent {
  private handleClick() {
    alert('Important menu');
  }
}
