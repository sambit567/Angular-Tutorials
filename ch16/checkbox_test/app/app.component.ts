import {Component} from '@angular/core';

@Component({
selector: 'app-root',
template: `

<md-checkbox [checked]='true'>
  Initially checked
</md-checkbox><br /><br />

<md-checkbox [(checked)]='isChecked' align='end'>
  Not initially checked
</md-checkbox><br /><br />

<md-checkbox [checked]='true' disabled='true'>
  Checked and disabled
</md-checkbox><br /><br />

<button [innerText]='msg' (click)='handleClick()'></button>
`})

export class AppComponent {

  // The button's message
  private msg = 'Check second button';

  // The state of the second checkbox
  private isChecked = false;

  private handleClick() {
    this.isChecked = !this.isChecked;
    this.msg = this.isChecked ?
      'Uncheck second button' : 'Check second button';
  }
}
