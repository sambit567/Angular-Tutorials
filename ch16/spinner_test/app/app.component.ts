import {Component} from '@angular/core';

@Component({
selector: 'app-root',
styles: ['td { padding: 15px; }'],
template: `
<table>
  <tr>
    <th>Determinate</th>
    <th>Indeterminate</th>
  </tr>

  <tr>
    <!-- Determinate spinner -->
    <td>
      <md-progress-spinner mode='determinate'
          color='accent' [value]='progress'>
      </md-progress-spinner>
    </td>
    
    <!-- Indeterminate spinner -->    
    <td>
      <md-progress-spinner mode='indeterminate' color='primary'>
      </md-progress-spinner>
    </td>
  </tr>
  
  <!-- Update determinate spinner -->  
  <tr>
    <td align='center'>
      <button md-raised-button (click)='handleClick()'>
        Update
      </button>
    </td>
  </tr>
</table>
`})

export class AppComponent {

  // The degree of progress
  private progress = 0;

  // Advance progress by 10%
  private handleClick() {
    this.progress = (this.progress + 10) % 100;
    console.log('Progress: ' + this.progress);
  }
}
