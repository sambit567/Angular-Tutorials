import {Component} from '@angular/core';

// Define the component's markup
@Component({
selector: 'app-root',
styles: ['td { padding: 15px; }'],
template: `
<table>
  <tr>
    <td>Buttons:</td>
    <td><button md-button>Regular</button></td> 
    <td><button md-button disabled='true'>Disabled</button></td>
  </tr>
  <tr>
    <td>Raised Buttons:</td>
    <td><button md-raised-button>Regular</button></td> 
    <td><button md-raised-button color='warn'>Warn</button></td>
  </tr>
  <tr>
    <td>Icon Buttons:</td>
    <td><button md-icon-button><img src='assets/images/cross.png'></button></td> 
    <td><button md-icon-button><img src='assets/images/error.png'></button></td> 
  </tr>  
  <tr>
    <td>Floating Action Button (FAB):</td>
    <td><button md-fab><img src='assets/images/fab.png'></button></td> 
  </tr>  
  <tr>
    <td>Mini-FAB:</td>
    <td><button md-mini-fab><img src='assets/images/fab.png'></button></td> 
  </tr>    
</table>
`})

// Define the component's controller
export class AppComponent {}
