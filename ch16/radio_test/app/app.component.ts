import {Component} from '@angular/core';

// Define the component's markup
@Component({
selector: 'app-root',
styles: ['md-radio-button { display: block; margin-top: 0px; margin-bottom: 10px; } '],
template: `
<label>Favorite Commedia dell'Arte Character: {{ selection }}</label>
<p>
<md-radio-group [(ngModel)]='selection'>
  <md-radio-button *ngFor='let char of chars' [value]='char'>
    {{ char }}
  </md-radio-button>
</md-radio-group>
</p>
<button type='button' (click)='changeSelection()'>Select Scaramouche</button>
`})

// Define the component's controller
export class AppComponent {

  public chars = ['Scaramouche', 'Pulcinella', 'Pantalone', 'Columbine', 'il Dottore'];
  public selection = 'Pulcinella';

  // Update selection
  private changeSelection() {
    this.selection = 'Scaramouche';
  }
}
