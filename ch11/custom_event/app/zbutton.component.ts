import {Component, EventEmitter, Output} from '@angular/core';

@Component({
selector: 'zbutton',
template: `
<button (keypress)='keyhandler($event)'>
  Press z to Generate Event
</button>
`})
export class ZButtonComponent {

  // Create the EventEmitter
  @Output() private zpress = new EventEmitter();

  // Check if the user pressed 'z'
  private keyhandler(e: any) {

    // If so, emit the custom event
    if (e.which === 122) {
      this.zpress.emit(e);
      alert('Custom event emitted');
    }
  }
}
