import {Component} from '@angular/core';

import {ZButtonComponent} from './zbutton.component';

@Component({
selector: 'app-root',
template: `
<zbutton (zpress)='handler()'></zbutton>
`})
export class AppComponent {

  // Respond to the custom event
  private handler() {
    alert('Custom event received');
  }
}
