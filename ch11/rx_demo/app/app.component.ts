import {Component} from '@angular/core';
import * as Rx from 'rxjs/Rx';

// First observer class
class Observer1 implements Rx.Observer<number> {
  public next(value: any) { console.log('Obs1.next: ' + value); };
  public error(err: any) { console.log('Obs1.error: ' + err); };
  public complete() { console.log('Obs1.complete'); };
}

// Second observer class
class Observer2 implements Rx.Observer<number> {
  public next(value: any) { console.log('Obs2.next: ' + value); };
  public error(err: any) { console.log('Obs2.error: ' + err); };
  public complete() { console.log('Obs2.complete'); };
}

@Component({
selector: 'app-root',
template: `
  <button (click) = 'createSubscriptions()'>
    Create Subscriptions</button>
  <p [innerHTML]='msg'></p>
`})
export class AppComponent {
  private obs: Rx.Observable<number>;
  private msg = '';

  constructor() {

    // Create observable
    this.obs = Rx.Observable.create(
      (ob: Rx.Subscriber<number>) => {
        try {
          ob.next(3); ob.next(2); ob.next(1);
          throw new Error('Problem');
        } catch (err) {
          ob.error(err);
        }
        return () => { this.msg += 'Terminate<br/><br/>'; };
      }
    );
  }

  // Subscribe the observers to the observable
  private createSubscriptions(): void {
    this.obs.subscribe(new Observer1());
    this.obs.subscribe(new Observer2());
  }
}
