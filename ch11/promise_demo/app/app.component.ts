import {Component} from '@angular/core';

@Component({
selector: 'app-root',
template: `
<p><b>First result</b> - {{ result1 }}</p>
<p><b>Second result</b> - {{ result2 }}</p>
`})

export class AppComponent {

  // State data: two promises and two results
  private promise1: Promise<number>;
  private promise2: Promise<void>;
  private promiseArray: Promise<any>[];
  private result1: string;
  private result2: string;

  constructor() {
    this.promise1 = new Promise((res, rej) => { res(123); });
    this.promise2 = Promise.reject('Big problem');

    // Process first promise
    new Promise((res, rej) => { res(123); }).then(
      (val: number) => { this.result1 =
        'Fulfilled with a value of ' + String(val); }).catch(
      (err: any) => { this.result1 =
        'Rejected with an error: ' + err.toString(); });

   // Process array of promises
    this.promiseArray = [this.promise1, this.promise2];
    Promise.all(this.promiseArray).then(
      (array: any[]) => { this.result2 =
        'Fulfilled with value: ' + array.toString(); }).catch(
      (error: any) => { this.result2 =
        'Rejected with an error of ' + error.toString(); });
  }
}
