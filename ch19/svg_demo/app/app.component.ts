import {Component} from '@angular/core';

// Define the component's markup
@Component({
selector: 'app-root',
styles: ['rect:hover { fill: whitesmoke !important; }'],
template: `
<svg width='100' height='40' (click)='handleClick()'>

  <!-- The rounded rectangle -->
  <rect x='3' y='3' width='94' height='34' rx='17' ry='17' 
    style='fill: gainsboro; stroke: gray; stroke-width: 5'/>

  <!-- The button's text -->
  <text x='16' y='25' font-family="Helvetica" 
    font-weight = "bold">Press Me</text>    
</svg>
`})
export class AppComponent {
  public handleClick() {
    alert('The button has been pressed');
  }
}
