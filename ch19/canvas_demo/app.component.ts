import {Component, ElementRef, AfterViewInit, ViewChild} from '@angular/core';

@Component({
selector: 'app-root',
styles: ['canvas { border: 1px solid; }'],
template: `
<canvas #test [width]='width' [height]='height'></canvas><br />
<button (click)='onClick()'>{{ keepDrawing ? 'Halt Animation' : 'Start Animation' }}</button>
`})

// Define the component's class
export class AppComponent implements AfterViewInit {

  @ViewChild('test') private canvas: ElementRef;
  private ctx: CanvasRenderingContext2D;
  private width = 200; private height = 200;
  private keepDrawing = true;
  private startTime: number;

  // Access view children
  public ngAfterViewInit() {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    window.requestAnimationFrame(t => this.drawFrame(t));
  }

  // Draw one frame
  private drawFrame(time: number) {

    // Get the elapsed time as a fraction of five seconds
    if (!this.startTime) {
      this.startTime = time;
    }
    const elapsedTime = (time - this.startTime) / 5000;

    // Set the initial transformation and clear the canvas
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.width, this.height);

    // Set the new transformation
    this.ctx.translate(100, 100);
    this.ctx.rotate(elapsedTime * 2 * Math.PI);

    // Draw the line
    this.ctx.beginPath();
    this.ctx.moveTo(0, 0);
    this.ctx.lineTo(50, 0);
    this.ctx.stroke();

    // Continue animation
    if (this.keepDrawing) {
      window.requestAnimationFrame(t => this.drawFrame(t) );
    }
  }

  private onClick() {
    this.keepDrawing = !this.keepDrawing;
    if (this.keepDrawing) {
      window.requestAnimationFrame(t => this.drawFrame(t) );
    }
  }
}
