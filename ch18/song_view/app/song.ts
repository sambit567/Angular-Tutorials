// The song class
export class Song {
  public id: string;
  public title: string;
  public artist: string;
  public year: string;
}
