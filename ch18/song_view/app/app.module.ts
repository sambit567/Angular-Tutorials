import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SongTableComponent } from './songtable/songtable.component';
import { EditPageComponent } from './editpage/editpage.component';

const routes: Routes = [
  {path: '', redirectTo: 'songtable', pathMatch: 'full'},
  {path: 'songtable', component: SongTableComponent},
  {path: 'editpage', component: EditPageComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    SongTableComponent,
    EditPageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
