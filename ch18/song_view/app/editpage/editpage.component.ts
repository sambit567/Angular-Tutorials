import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Http, Response} from '@angular/http';

import {Song} from '../song';

// Validates years: 4-digit numbers
function yearValidator(c: FormControl):
  {[errorCode: string]: boolean} {

  if (c.value.match(/^\d{4}$/g)) {
    return null;
  } else {
    return {'year': true};
  }
}

@Component({
selector: 'app-editpage',
templateUrl: 'editpage.component.html',
styleUrls: ['editpage.component.css']})
export class EditPageComponent implements OnInit {

  private song: Song;
  private c1 = new FormControl('', Validators.required);
  private c2 = new FormControl('', Validators.required);
  private c3 = new FormControl('',
      Validators.compose([Validators.required, yearValidator]));
  private group: FormGroup;
  private url = 'http://www.ngbook.io/songs/song';

  constructor(private http: Http) {

    // Create control group
    this.group = new FormGroup({
      'titleInput': this.c1,
      'artistInput': this.c2,
      'dateInput': this.c3});
  }

  public ngOnInit(): void {
    const id = window.location.search.substring(8);
    this.url = this.url.concat(id);

    // Send a GET request for the given song
    this.http.get(this.url)
      .map((res: Response) => res.json())
      .subscribe((song: Song) => {
        this.song = song;
        this.c1.setValue(song.title);
        this.c2.setValue(song.artist);
        this.c3.setValue(song.year);
      });
  }

  // Respond when the form is submitted
  private handleSubmit(): void {

    // Send a PUT request to update the song
    this.http.put(this.url, JSON.stringify(this.song))
      .subscribe((res: Response) => { alert(res.text()); });
  };
}
