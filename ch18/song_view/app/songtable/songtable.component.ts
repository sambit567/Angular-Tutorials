import {Component, Inject} from '@angular/core';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {RouterLink} from '@angular/router';
import 'rxjs/add/operator/map';

import {Song} from '../song';

// Enumerated type for sort direction
enum SortDirection {None, Asc, Desc};

@Component({
selector: 'app-songtable',
templateUrl: 'songtable.component.html',
styleUrls: ['songtable.component.css']})
export class SongTableComponent {
  private songs = [];
  private checks = [];
  private allChecked = false;
  private page = 0;
  private songsPerPage = 10;
  private numPages = 4;
  private sortDirection = SortDirection.None;
  private sortImage = 'assets/images/no_sort.png';
  private url = 'http://www.ngbook.io/songs';

  constructor(private http: Http) {
    this.sendRequest();
  }

  // Request records from the server
  private sendRequest(): void {
    // Construct query string
    let query = 'page='.concat(this.page.toString()).concat('&songsPerPage=').concat(this.songsPerPage.toString());
    switch (this.sortDirection) {
      case SortDirection.Asc:
        query = query.concat('&sort=asc');
        break;
      case SortDirection.Desc:
        query = query.concat('&sort=desc');
        break;
      default:
        break;
    }

    // Send a request and process the response
    const opts = {search: query};
    this.http.get(this.url, opts)
      .map((res: Response) => res.json())
      .subscribe((songs: Array<Song>) => this.songs = songs);
  }

  // Respond when all boxes are checked/unchecked
  private handleChecks(checked: boolean): void {
    if (checked) {
      for (let i = 0; i < this.songs.length; i++) {
        this.checks[i] = true;
      }
    } else {
      this.clearChecks();
    }
  }

  // Respond to text search
  private handleSearch(text: string): void {
    let query: string;

    if (text) {
      query = 'search='.concat(text);
    } else if (text === '') {
      query = 'page='.concat(this.page.toString()).concat('&songsPerPage=').concat(this.songsPerPage.toString());
    }
    const opts: RequestOptionsArgs = {search: query};
    this.http.get(this.url, opts)
      .map((res: Response) => res.json())
      .subscribe((songs: Array<Song>) => this.songs = songs);
  }

  // Respond when a different page count is selected
  private handleSelect(selection: string): void {
    this.songsPerPage = parseInt(selection, 10);
    this.numPages = Math.round(40 / this.songsPerPage);
    if (this.page >= this.numPages) {
      this.page = this.numPages - 1;
    }
    this.sendRequest();
    this.clearChecks();
  }

  // Respond when the delete button is pressed
  private handleDelete(): void {
    let deleteURL: string;
    for (let i = 0; i < this.songs.length; i++) {
      if (this.checks[i]) {

        // Update the URL and send the DELETE request
        deleteURL = this.url.concat('/song').concat(this.songs[i].id);
        this.http.delete(deleteURL)
          .subscribe((res: Response) => { alert(res.text()); });
      }
      this.checks[i] = false;
    }
  }

  // Respond to record sort
  private handleSort(): void {
    switch (this.sortDirection) {
      case SortDirection.None:
        this.sortDirection = SortDirection.Desc;
        this.sortImage = 'assets/images/desc_sort.png';
        break;
      case SortDirection.Desc:
        this.sortDirection = SortDirection.Asc;
        this.sortImage = 'assets/images/asc_sort.png';
        break;
      case SortDirection.Asc:
        this.sortDirection = SortDirection.None;
        this.sortImage = 'assets/images/no_sort.png';
        break;
    }
    this.sendRequest();
  }

  // Respond when the previous link is clicked
  private handlePrev(): void {
    this.page--;
    this.sendRequest();
    this.clearChecks();
  }

  // Respond when the next link is clicked
  private handleNext(): void {
    this.page++;
    this.sendRequest();
    this.clearChecks();
  }

  // Uncheck the checkboxes
  private clearChecks(): void {
    this.allChecked = false;
    for (let i = 0; i < this.songs.length; i++) {
      this.checks[i] = false;
    }
  }
}
